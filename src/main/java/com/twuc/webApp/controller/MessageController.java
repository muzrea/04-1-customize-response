package com.twuc.webApp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {
    @GetMapping("/api/no-return-value")
    void getNoReturnValue(){
    }

    @GetMapping("/api/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void getNoReturnValueWithAnnotation(){
    }

    @GetMapping("/api/messages/{message}")
    String getPathVariableValue(@PathVariable String message){
        return message;
    }

    @GetMapping("/api/messages-objects/{message}")
    Message getObjectPathVariableValue(@PathVariable String message){
        return new Message(message);
    }

    @GetMapping("/api/message-objects-with-annotation/{message}")
    ResponseEntity<Message> getObjectAndStatus(@PathVariable String message){
        Message wellMessage = new Message(message);
        return ResponseEntity.status(202)
                .body(wellMessage);
    }

    @GetMapping("/api/message-entities/{message}")
    ResponseEntity<Message> getObjectHeaderStatus(@PathVariable String message){
        Message wellMessage = new Message(message);
        return ResponseEntity.status(200)
                .header("X-Auth","me")
                .body(wellMessage);
    }
}
