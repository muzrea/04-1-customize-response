package com.twuc.webApp.controller;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;

public class Message {
    private final String value;

    public Message(String value) {
        this.value = value;
    }

//    @JsonValue
    public String getValue() {
        return value;
    }
}
