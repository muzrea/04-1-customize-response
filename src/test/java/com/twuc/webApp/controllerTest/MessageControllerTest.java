package com.twuc.webApp.controllerTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.controller.Message;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class MessageControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_return_200_when_api_with_no_return() throws Exception {
        mockMvc.perform(get("/api/no-return-value"))
                .andExpect((status().is(200)));
    }

    @Test
    void should_return_204_when_api_with_no_return_with_annotation() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation"))
                .andExpect((status().is(204)));
    }

    @Test
    void should_return_200_when_api_return_string() throws Exception {
        mockMvc.perform(get("/api/messages/sss"))
                .andExpect((status().is(200)))
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("sss"));
    }

    @Test
    void should_return_message_when_api_return_message() throws Exception {
        Message message = new Message("well");
        String messageString = new ObjectMapper()
                .writeValueAsString(message);
        mockMvc.perform(get("/api/messages-objects/well"))
                .andExpect((status().is(200)))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().string(messageString)).andDo(print());
    }

    @Test
    void should_return_message_and_202_when_api_return_message() throws Exception {
        Message message = new Message("well");
        String messageString = new ObjectMapper()
                .writeValueAsString(message);
        mockMvc.perform(get("/api/message-objects-with-annotation/well"))
                .andExpect((status().is(202)))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().string(messageString));
    }

    @Test
    void should_return_message_and_200_and_header_when_api_return_message() throws Exception {
        Message message = new Message("well");
        String messageString = new ObjectMapper()
                .writeValueAsString(message);
        mockMvc.perform(get("/api/message-entities/well"))
                .andExpect((status().is(200)))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(header().stringValues("me"))
                .andExpect(content().string(messageString));
    }
}
